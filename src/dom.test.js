import * as app from "./dom.js";
import defaultApp from "./dom.js";

describe('simple func',()=>{
  it('outpend', () => {
    const root = document.createElement('div');
    const originalParent = document.createElement('div');
    const child = document.createElement('div');
    const inserted = document.createElement('div');
    root.appendChild(originalParent);
    originalParent.appendChild(child);
    app.outpend(originalParent,inserted);

    expect( child.parentNode ).toBe(originalParent);
    expect( originalParent.parentNode ).toBe(inserted);
    expect( inserted.parentNode ).toBe(root);
  });
  //TODO test left()
  describe('buildNode', () => {
    function template(tag, expectedVal, checkWhere, setContent = false) {
      const node = setContent ? defaultApp(tag, 'Lorem ipsum') : app.buildNode(tag);
      expect(node[checkWhere]).toBe(expectedVal);
    }

    it('build node of asked type', () => template('p', 'p', 'localName'));
    it('no content -> no content', () => template('div', '', 'innerText'));
    it('build usual node with content', () => template('p', 'Lorem ipsum', 'innerText', true));
    it("build input node and don't translate data", () => template('input', 'Lorem ipsum', 'value', true));
    it("build div by default", () => template(undefined, 'div', 'localName'));
    it("add class", () => template('.toto', 'toto', 'className'));
    it("add classes", () => template('a.toto.tata', 'toto tata', 'className'));
    it("add id", () => template('#toto', 'toto', 'id'));
    it("add id with classes (check id)", () => template('a.plop#toto.plip.plouf', 'toto', 'id'));
    it("add id with classes (check classes)", () => template('a.plop#toto.plip.plouf', 'plop plip plouf', 'className'));
    it("add id with classes (check tagName)", () => template('a.plop#toto.plip.plouf', 'a', 'localName'));
    it("add id with classes tag not specified(check tagName)", () => template('.plop#toto.plip.plouf', 'div', 'localName'));
  });
  describe('addNodeTo variations', () => {
    let id, me, here;
    beforeEach(() => {
      here = document.createElement('section');
      id = "myId";
      me = app.buildNode('div');
      me.id = id;
    });
    describe("add new node", () => {
      function template(func) {
        expect(here.querySelector("div#" + id)).toBeFalsy();
        func(me, here);
        expect(here.querySelector("div#" + id)).toBeTruthy();
      }

      it('with addOrReplace', () => template(app.addOrReplace));
      it('with addOnce', () => template(app.addOnce));
    });
    describe("interact with allready taken id", () => {
      beforeEach(() => {
        app.addOnce(me, here);
        me = app.buildNode('span');
        me.id = id;
      });
      function template(func, tagTrue, tagFalse) {
        func(me, here);
        expect(here.querySelector(tagTrue + "#" + id)).toBeTruthy();
        expect(here.querySelector(tagFalse + "#" + id)).toBeFalsy();
      }

      it("don't replace existing node when use addOnce", () => template(app.addOnce, "div", "span"));
      it("replace existing node when use addOrReplace", () => template(app.addOrReplace, "span", "div"));
    });
  });
});
/*describe('dom factory',()=>{
});
*/
