# Tipǎy

Vendez, à prix fixe, modulable ou libre, et incitez aux pourboires qui font sens à vos yeux.

Module de préparation au paiement (panier récapitulatif) intégrable à votre site en quelques lignes.

## [Demo et exemples](https://g1.frama.io/tipay/) ([ancienne url](https://tipay.duniter.io))

## Intégrer Tipǎy à votre site

### Méthode 1 : En quelques lignes

Sur votre site, en html ajoutez où vous voulez :
```html
  <link rel="stylesheet" href="https://tipay.duniter.io/tipay.css"/>
  <script defer src="https://tipay.duniter.io/tipay.js"></script>
```

et là ou vous souhaitez insérer un encart Tipǎy :
```html
  <template class="tipay">
    { "title":"La config de votre encart Tipǎy ici" },
  </template>
```
Des exemples de configurations son disponbile : [rendu visuel](https://g1.frama.io/tipay/), [config correspondante](https://framagit.org/g1/tipay/blob/master/src/demo.html)

### Méthode 2 : Intégration complète (sans liens externes)

- `npm install tipay`
- là où vous voulez un encart Tipǎy
```html
  <template class="tipay">
    { "title":"La config de votre encart Tipǎy ici" },
  </template>
```
- à la main ou via un packer : 
```html
  <link rel="stylesheet" href="whereYouWant/tipay.css"/>
  <script defer src="whereYouWant/tipay.js"></script>
```
Vous voici avec Tipǎy sans liens vers un autre site. (et donc testable en local hors connexion)

<!--
- Bonus `tipay file.html` pour générer une version noscript de l'encart Tipǎy avec quelques liens pour finaliser sans js.
-->

## Options / configuration
